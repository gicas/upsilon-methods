from upsilon import upsilon62, upsilon63, upsilon63c
import numpy as np
import numpy.testing as npt
import scipy as sp
import unittest


class TestAutonomous(unittest.TestCase):

    def setUp(self):
        self.Tf = sp.pi
        self.n_steps = 100
        self.n = 10
        self.A = np.random.rand(self.n, self.n)
        self.M = lambda t: self.A
        self.AA = np.array(np.bmat(
            [[np.zeros((self.n, self.n)), np.eye(self.n, self.n)],
                [self.A, np.zeros((self.n, self.n))]]))
        self.v = np.random.rand(self.n, 1)
        self.w = np.random.rand(self.n, 1)
        self.expected = sp.linalg.expm(
            self.AA * self.Tf)@np.vstack((self.v, self.w))

    def test_upsilon62(self):
        result = upsilon62((self.M, self.M), (self.v, self.w), [
                           0, self.Tf], self.n_steps)()
        npt.assert_allclose(result, self.expected)

    def test_upsilon63(self):
        result = upsilon63((self.M, self.M), (self.v, self.w), [
                           0, self.Tf], self.n_steps)()
        npt.assert_allclose(result, self.expected)

    def test_upsilon63c(self):
        result = upsilon63((self.M, self.M), (self.v, self.w), [
                           0, self.Tf], self.n_steps)()
        npt.assert_allclose(result, self.expected)


test_suite = unittest.TestSuite()
test_suite.addTest(unittest.makeSuite(TestAutonomous))
runner = unittest.TextTestRunner()
runner.run(test_suite)
