from expms import expmkr
import krypy as kp
import numpy as np
import scipy as sp
from scipy import linalg as la


def eyes(shp):
    m, n = shp
    return np.eye(m, n)


def block(lst):
    return np.array(np.bmat(lst))


class UpsilonMethod:

    def __init__(self, sys, iv, t_span, n_steps):
        self.g, self.f = sys
        self.ivf, self.ivg = iv
        self.t0, self.tf = t_span
        self.n_steps = n_steps
        self.h = (self.tf - self.t0) / n_steps
        self.Id = eyes(self.g(0).shape)
        self.O = np.zeros(self.g(0).shape)
        self.t = self.t0
        self.gl = [0.5 - np.sqrt(15.0) / 10.0, 0.5, 0.5 + np.sqrt(15.0) / 10.0]

    def __call__(self):
        return self.integrate()

    def step(self, t):
        raise NotImplementedError()

    def integrate(self):
        (V, W) = self.ivf, self.ivg
        for s in range(self.n_steps):
            (V, W) = self.step((V, W), self.t)
        return np.vstack((V, W))


class upsilon62(UpsilonMethod):
    name = r'$\Upsilon_{2}^{[6]}$'
    matcost = 2. * (7. + 8. + 1. / 3.) + 1

    def __init__(self, *args, **kwargs):
        super(upsilon62, self).__init__(*args, **kwargs)

    def step(self, iv, t):
        M1 = self.g(t + self.gl[0] * self.h)
        M2 = self.g(t + self.gl[1] * self.h)
        M3 = self.g(t + self.gl[2] * self.h)

        K = -M1 + M3
        L = M1 - 2 * M2 + M3
        F = self.h * self.h * K * K

        C1 = (-sp.sqrt(15.0) / 180.0) * K + \
            (1.0 / 18.0) * L + (1.0 / 12960.0) * F
        C2 = (+sp.sqrt(15.0) / 180.0) * K + \
            (1.0 / 18.0) * L + (1.0 / 12960.0) * F
        D1 = M2 - (4.0 / (3 * sp.sqrt(15.0))) * K + (1.0 / 6.0) * L
        D2 = M2 + (4.0 / (3 * sp.sqrt(15.0))) * K + (1.0 / 6.0) * L

        (V, W) = iv

        W = self.h * np.dot(C1, V) + W
        T1 = 0.5 * self.h * block([[self.O, self.Id], [D1, self.O]])
        result = la.expm(T1) @ np.vstack((V, W))
        T2 = 0.5 * self.h * block([[self.O, self.Id], [D2, self.O]])
        result = la.expm(T2) @ result
        half = len(result) // 2
        V = result[:half, :]
        W = result[half:, :]
        W = self.h * np.dot(C2, V) + W

        self.t = t + self.h
        return (V, W)


class upsilon63(UpsilonMethod):
    name = r'$\Upsilon_{3}^{[6]}$'
    matcost = 3. * (7. + 8. + 1. / 3.) + 1

    def __init__(self, *args, **kwargs):
        super(upsilon63, self).__init__(*args, **kwargs)

    def step(self, iv, t):
        x = [0.74124492932331537426251171712,  # x12 x0
             0.666325345008161097663344931364,  # x21 x1
             -2.47120436125667515183058990701,  # x22 x2
             -0.144268304407230768054800445782,  # x23 x3
             -0.332650690016322195326689862728,  # x31 x4
             0.371869942147794869442934224898]  # x33 x5

        # Philipp
        x = [0.015446203250883929564,
             0.56704071886547742757,
             0.15679795546721757294,
             0.085748160282456073452,
             -0.13408143773095485515,
             -0.088162987231578813572]

        M1 = self.g(t + self.gl[0] * self.h)
        M2 = self.g(t + self.gl[1] * self.h)
        M3 = self.g(t + self.gl[2] * self.h)

        K = (-M1 + M3)
        L = (M1 - 2 * M2 + M3)

        C1 = -sp.sqrt(5. / 3.) * x[0] * K
        C2 = sp.sqrt(5. / 3.) * x[0] * K
        D1 = M2 - sp.sqrt(5. / 3.) * x[2] / x[1] * K + \
            (10. / 3.) * x[3] / x[1] * L
        D2 = M2 + (10. / 3) * x[5] / x[4] * L
        D3 = M2 + sp.sqrt(5. / 3.) * x[2] / x[1] * K + \
            (10. / 3.) * x[3] / x[1] * L

        (V, W) = iv

        W = self.h * np.dot(C1, V) + W
        T1 = x[1] * self.h * block([[self.O, self.Id], [D1, self.O]])
        result = la.expm(T1) @ np.vstack((V, W))
        T2 = x[4] * self.h * block([[self.O, self.Id], [D2, self.O]])
        result = la.expm(T2) @ result
        T3 = x[1] * self.h * block([[self.O, self.Id], [D3, self.O]])
        result = la.expm(T3) @ result
        half = len(result) // 2
        V = result[:half, :]
        W = result[half:, :]
        W = self.h * np.dot(C2, V) + W

        self.t = t + self.h
        return (V, W)


class upsilon63c(UpsilonMethod):
    name = r'$\Upsilon_{3c}^{[6]}$'
    matcost = 2. * (7. + 8. + 1. / 3.) + 1 + 10

    def __init__(self, *args, **kwargs):
        super(upsilon63c, self).__init__(*args, **kwargs)

    def step(self, iv, t):
        # x = [0.0306002309434948967887611197341,  # x12 x0
        #      1.0 / 60.0,                         # x13 x1
        #      0.5,                                # x21 x2
        #      0.0775990762260204128449555210635,  # x22 x3
        #      1.0 / 40.0,                         # x23 x4
        #      -0.0139335642768282301220944530675]  # c11 x5
        #
        # x = [0.0148543145110505577566934257204,
        #      0.0166666666666666666666666666667,
        #      0.500000000000000000000000000000,
        #      0.140582741955797768973226297118,
        #      0.0250000000000000000000000000000,
        #      0.00181235215561610890997324094624]

        #  Philipp
        x = [-0.014854314511050557757,  # x12 x0
             1.0 / 60.0,                         # x13 x1
             0.5,                                # x21 x2
             -0.14058274195579776897,  # x22 x3
             1.0 / 40.0,                         # x23 x4
             0.0018123521556161089100]  # c11 x5

        M1 = self.g(t + self.gl[0] * self.h)
        M2 = self.g(t + self.gl[1] * self.h)
        M3 = self.g(t + self.gl[2] * self.h)

        K = -M1 + M3
        L = M1 - 2 * M2 + M3

        C2 = -sp.sqrt(5. / 3) * x[0] * K + (10. / 3) * x[1] * L
        D2 = M2 - sp.sqrt(5. / 3) * x[3] / x[2] * \
            K + (10. / 3) * x[4] / x[2] * L
        E = np.sqrt(5. / 3) * x[5] * self.h * self.h * \
            block([[K, self.O], [self.O, -K]])
        D1 = M2 + sp.sqrt(5. / 3) * x[3] / x[2] * \
            K + (10. / 3) * x[4] / x[2] * L
        C1 = sp.sqrt(5. / 3) * x[0] * K + (10. / 3) * x[1] * L

        (V, W) = iv

        W = self.h * np.dot(C1, V) + W
        T1 = self.h * x[2] * block([[self.O, self.Id], [D1, self.O]])
        result = la.expm(T1) @ np.vstack((V, W))
        result = la.expm(E) @ result
        T2 = self.h * x[2] * block([[self.O, self.Id], [D2, self.O]])
        result = la.expm(T2) @ result
        half = len(result) // 2
        V = result[:half, :]
        W = result[half:, :]
        W = self.h * np.dot(C2, V) + W

        self.t = t + self.h
        return (V, W)


class upsilon62kr(UpsilonMethod):
    name = r'$\Upsilon_{2kr}^{[6]}$'
    matcost = 32. + 2 / 3

    def __init__(self, *args, **kwargs):
        super(upsilon62kr, self).__init__(*args, **kwargs)

    def step(self, iv, t):
        M1 = self.g(t + self.gl[0] * self.h)
        M2 = self.g(t + self.gl[1] * self.h)
        M3 = self.g(t + self.gl[2] * self.h)

        K = -M1 + M3
        L = M1 - 2 * M2 + M3
        F = self.h * self.h * K * K

        C1 = (-sp.sqrt(15.0) / 180.0) * K + \
            (1.0 / 18.0) * L + (1.0 / 12960.0) * F
        C2 = (+sp.sqrt(15.0) / 180.0) * K + \
            (1.0 / 18.0) * L + (1.0 / 12960.0) * F
        D1 = M2 - (4.0 / (3 * sp.sqrt(15.0))) * K + (1.0 / 6.0) * L
        D2 = M2 + (4.0 / (3 * sp.sqrt(15.0))) * K + (1.0 / 6.0) * L

        (V, W) = iv

        W = self.h * np.dot(C1, V) + W
        T1 = 0.5 * self.h * block([[self.O, self.Id], [D1, self.O]])
        result = expmkr(T1, np.vstack((V, W)))
        T2 = 0.5 * self.h * block([[self.O, self.Id], [D2, self.O]])
        result = expmkr(T2, result)
        half = len(result) // 2
        V = result[:half, :]
        W = result[half:, :]
        W = self.h * np.dot(C2, V) + W

        self.t = t + self.h
        return (V, W)
