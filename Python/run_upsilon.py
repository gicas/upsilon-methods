import krypy as kp
import numpy as np
from upsilon import upsilon62, upsilon63, upsilon63c, expmkr
from scipy import linalg as la
np.set_printoptions(precision=4)
np.set_printoptions(suppress=True)


def nonautwave(x, eps, delta):
    h = np.abs(x[0] - x[1])
    n = len(x)
    L = np.diagflat(-2 * np.ones((n, 1)), 0) + \
        np.diagflat(np.ones((n - 1, 1)), 1) + \
        np.diagflat(np.ones((n - 1, 1)), -1) + \
        np.diagflat(1, n - 1) + np.diagflat(1, -n + 1)
    L = L / (h * h)
    W = lambda t: L - np.diagflat(x**2 + eps * np.cos(delta * t) * x**2)
    return W


n = 10
X0 = -10
Xf = 10
x = np.linspace(X0, Xf, num=n)
sz = (n, n)

#  time
T0 = 0
Tf = 1
steps = 100

v = np.exp(-0.5 * x**2).reshape(-1, 1)
w = 0.0 * v
iv = np.vstack((v, w))

M = nonautwave(x, 1, 1)
Id = np.identity(n)
O = np.zeros(sz)
A = lambda t: np.array(np.bmat([[O, Id], [-M(t), O]]))
# ref = la.expm(A * (Tf - T0))@iv
B = A(0)
m = 20
# ea1 = la.expm(B)@iv
# ea2, er = expmkr(B, iv, m)
# ea3 = la.expm3(B, m)@iv
# print(la.norm(ea1 - ea2))
# print(er)
# print(la.norm(ea1 - ea3))

u62 = upsilon62((M, Id), (v, w), [T0, Tf],  steps)()
u62
# u63 = upsilon63((M, Id), (v, w), [T0, Tf], 4 * steps)()
# u63c = upsilon63c((M, Id), (v, w), [T0, Tf], 4 * steps)()
#
# print(la.norm(u62 - u63))
# print(la.norm(u62 - u63c))
