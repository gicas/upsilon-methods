import krypy as kp
import numpy as np
from scipy.linalg import norm, expm, expm3

n = 8
arr = np.array(range(1, 1 + n * n))

M = arr.reshape(n, n)
nm = norm(M)
M = M / (nm**0.75)
S = M@M.T
Id = np.identity(n)
O = np.zeros((n, n))

A = np.array(np.bmat([[O, Id], [-M, O]]))
v = np.random.rand(2 * n, 1)

ref = expm(A) @ v
V, H = kp.utils.arnoldi(A, v, 6)
tmp = norm(v) * V @ expm(H)
kr = tmp[:, [0]]
norm(kr-ref)

Arn = kp.utils.Arnoldi(A, v, 20)
Arn.advance()
b = Arn.get()
