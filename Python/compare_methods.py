from upsilon import upsilon62, upsilon63, upsilon63c
import numpy as np
import scipy as sp
import scipy.linalg as la
import matplotlib.pyplot as plt


def nonautwave(x, eps, delta):
    h = np.abs(x[0] - x[1])
    n = len(x)
    L = np.diagflat(-2 * np.ones((n, 1)), 0) + \
        np.diagflat(np.ones((n - 1, 1)), 1) + \
        np.diagflat(np.ones((n - 1, 1)), -1) + \
        np.diagflat(1, n - 1) + np.diagflat(1, -n + 1)
    L = L / (h * h)

    def W(t):
        return L - np.diagflat(x**2 + eps * np.cos(delta * t) * x**2)
    return W


def cost_error(method, margs, reference, n_steps):
    cost = method.matcost * n_steps
    result = method(*margs, n_steps)()
    error = la.norm(reference - result)
    yield cost, error


n = 32
X0 = -10
Xf = 10
x = np.linspace(X0, Xf, num=n)
sz = (n, n)
#  time
T0 = 0
Tf = sp.pi
#  system
eps = 0.5
delta = 0.75
M = nonautwave(x, eps, delta)
# initial valuвs
v = np.exp(-0.5 * x**2).reshape(-1, 1)
w = 0.0 * v
iv = np.vstack((v, w))

methods = [upsilon63, upsilon63c, upsilon62]
args = ((M, M), (v, w), [T0, Tf])
reference = upsilon62(*args, n_steps=500)()


steps = np.rint([10**k for k in np.arange(0, 2, 0.5)]).astype(int)
errors = np.empty((len(methods), len(steps)))
costs = np.empty((len(methods), len(steps)))
for idm, m in enumerate(methods):
    for ids, s in enumerate(steps):
        cost, error = list(cost_error(m, args, reference, s))
        # result = m(*args, n_steps=s)()
        errors[idm, ids] = error
        costs[idm, ids] = cost

plt.plot(np.log10(steps), np.log10(errors).T)
plt.legend(['b', 'c', 'd'])
plt.show()
