import krypy as kp
import numpy as np
from upsilon import upsilon62, upsilon63
from scipy import linalg as la

np.set_printoptions(precision=4)

n = 6
sz = (n, n)

Id = np.identity(n)
O = np.zeros(sz)

#  system's matrix
R = np.array(range(n * n)).reshape(n, n)
D = np.diagflat(np.ones((n, 1)) * n * n, 0)
M = (np.dot(R.T, R) + D) / la.norm(R, 2)
N = -M
A = np.bmat([[O, Id], [N, O]])
v = np.random.rand(n, 1)
w = np.random.rand(n, 1)
iv = np.vstack((v, w))

#  time
T0 = 0
Tf = 3

ref = la.expm(A*(Tf-T0))@iv
ups62 = upsilon62(Id, N, [T0, Tf], v, w, 1000)
ups63 = upsilon63(Id, N, [T0, Tf], v, w, 1000)

print(la.norm(ref - ups62))
print(la.norm(ref - ups63))
