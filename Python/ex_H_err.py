import numpy as np
from numpy import matlib
import krypy as kp
from scipy import linalg as la
from scipy.interpolate import *
from math import *
from upsilon import *
from matplotlib.pyplot import *
import pandas


np.set_printoptions(precision=2)

n = 128
sz =(n,n)

Id = np.matlib.identity(n)
O = np.matlib.zeros(sz)

R = np.matlib.rand(sz)
D = np.diagflat(np.matlib.rand(n,1)*n*n,0)
M = 0.001*R.T @ R + D
N = -M

A = np.bmat([[O,Id],[N,O]])
H = np.bmat([[M,O],[O,Id]])

def ham(v):
    return v.T @ H @ v
def hdiff(v,w):
    return la.norm(ham(v)-ham(w))

x = np.linspace(-10,10,n).T
vg = np.exp(-0.5*x**2).reshape(n,1)
wg = -0.01*vg;
#print(w)
ivg = np.concatenate((vg,wg))
#print(ivg)
ivr = np.matlib.rand(n*2,1)

T0=0
Tf =100*pi
t_span = [T0,Tf]
n_steps = 20;
h = (Tf-T0)/n_steps

# добавить случайный вектор
vk = vg;
wk = wg;
vu = vg;
wu = wg;
ers = np.zeros((n_steps,2))

expmkr(A,ivr,20)
for i in range(n_steps):
    ti = i*h
    tf = (i+1)*h

    ref = la.expm(tf*A.A)@ivg

    skr = upsilon62krylov(N, N, [ti,tf], vk, wk, 1, 12)
    vk, wk = np.vsplit(skr,2)

    sups = upsilon62(N, N, [ti,tf], vu, wu, 1)
    vu, wu = np.vsplit(sups,2)

    ers[i,0]=la.norm(ref-skr)
    ers[i,1]=la.norm(ref-sups)

t = np.linspace(T0,Tf,n_steps)
y=np.log10(ers+10.e-20)
#s = interp1d(t,y,kind='cubic')
#y=pandas.rolling_mean(y,5)
plot(t,y)


show()
