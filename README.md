## Upsilon splitting methods
This is an implementation of the methods described in *Bader, P., Blanes, S., 
Ponsoda, E. & Seydaoğlu, M. Symplectic integrators for the matrix Hill’s equation 
and its applications to engineering models. (2015)*.

Each method is straightforward and non-optimized.

## Usage
Git-fetch or copy the m-files.

