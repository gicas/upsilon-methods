function result = upsilon62( ...
    F_PLCHLD, G, t_span, iv_F, iv_G, n_steps)
% UPSILON62 solves a DE system using 6th-order with 2 expms.
%   UPSILON62 solves a DE system split into I and G: [O,I;G,O] or, e.g.,
%   I and G:=-M: [O,I;-M,O], where M is symm and pos def.
%
%   This is an example implementation for illustrating the articles:%
%   1) Bader, P., Blanes, S., Ponsoda, E. & Seydaoğlu, M.
%   Symplectic integrators for the matrix Hill’s equation and its applications
%   to engineering models. (2015). at <http://arxiv.org/abs/1512.02343>
%   2) Bader, P., Blanes, S., Casas, F. Kopylov, N., Ponsoda, E.
%   Symplectic integrators for time-dependent linear wave equations. (2016).

% Gauss - Legendre quadrature points
c1 = 0.5 - sqrt(15.0) / 10.0;
c2 = 0.5;
c3 = 0.5 + sqrt(15.0) / 10.0;

G = any2fun(G);
I = eye(size(G(0)));
O = zeros(size(G(0)));

t0 = t_span(1);
tf = t_span(end);
h = (tf - t0) / n_steps;

V = iv_F;
W = iv_G;

for step = 1:n_steps
    t = t0 + (step - 1) * h;
    
    M1 = G(t+c1*h);
    M2 = G(t+c2*h);
    M3 = G(t+c3*h);
    
    K = -M1 + M3;
    L = M1 - 2 * M2 + M3;
    F = h * h * K * K;
    C1 = (-sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
    C2 = (+sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
    D1 = M2 - (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
    D2 = M2 + (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
    
    W = h * C1 * V + W;
    result = expm(0.5*h*[[O, I]; [D2, O]]) * expm(0.5*h*[[O, I]; [D1, O]]) * [V; W];
    [V, W] = splitvec(result);
    W = h * C2 * V + W;
    
end
result = [V; W];
% end of function
end
