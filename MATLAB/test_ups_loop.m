clear all;
format long;

nx = 16;

R = rand(nx, nx);
r = norm(R, 2);
dg = rand(nx, 1) * nx;
tmp = R' * R + diag(dg);
M = tmp / norm(tmp);
N = -M;
A = [zeros(size(M)), eye(size(M)); ...
    N, zeros(size(M))];
B = [M, zeros(size(M)); ...
    zeros(size(M)), eye(size(M))];
I = eye(size(M));
h = 0.1;

x0 = -10;
xf = 10;
x = linspace(x0, xf, nx)';

v = exp(-0.5*x.^2);
w = -0.01 * v;

n = 100;
err = zeros(n, 2);
%r1 = upsilon62(A, A, [0, n * h], v, w, n);

v1 = v;
w1 = w;
iv = [v; w];
for i = 1:n
    ti = (i - 1) * h;
    tf = i * h;
    ref = expm(tf*A) * [v; w];
    s1 = upsilon62krylov(I, N, [ti, tf], v1, w1, 1, 16);
    [v1, w1] = splitvec(s1);
    s2 = upsilon62krylov(I, N, [0, tf], v, w, i, 16);
    %     err(i,1) = norm(s1'*B*s1 - iv'*B*iv, 2);
    %     err(i,2) = norm(s2'*B*s2 - iv'*B*iv, 2);
    err(i, 1) = norm(s1-ref, 2);
    err(i, 2) = norm(s2-ref, 2);
end

plot(err)