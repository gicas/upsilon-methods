function test_suite = test_of_upsilon
clear all;
try % assignment of 'localfunctions' is necessary in Matlab >= 2016
    test_functions = localfunctions();
catch % no problem; early Matlab versions can use initTestSuite fine
end
initTestSuite;
end

function test_upsilon62
% reference obtained with: upsilon62(@(t) magic(2),[0,1],eye(4),100)
reference = [2.11697074177913, 1.96102704701377, 1.28551976108179, 0.587063762445178; ...
    2.61470272935170, 2.77064642411709, 0.782751683260236, 1.48120768189687; ...
    3.63377481086250, 5.03068680813577, 2.11697074177916, 1.96102704701379; ...
    6.70758241084765, 5.31067041357445, 2.61470272935169, 2.77064642411709];

d = 2;
result = upsilon62(@(t)eye(d), @(t)magic(d), [0, 1], [eye(d), zeros(d)], [zeros(d), eye(d)], 10);
assertElementsAlmostEqual(result, reference);
end

function test_upsilon62dec
% reference obtained with: upsilon62(@(t) magic(2),[0,1],eye(4),100)
reference = [2.11697074177913, 1.96102704701377, 1.28551976108179, 0.587063762445178; ...
    2.61470272935170, 2.77064642411709, 0.782751683260236, 1.48120768189687; ...
    3.63377481086250, 5.03068680813577, 2.11697074177916, 1.96102704701379; ...
    6.70758241084765, 5.31067041357445, 2.61470272935169, 2.77064642411709];

d = 2;
result = upsilon62(@(t)eye(d), @(t)magic(d), [0, 1], [eye(d), zeros(d)], [zeros(d), eye(d)], 100, 8);

assertElementsAlmostEqual(result, reference);
end

function test_upsilon41
% reference obtained with: upsilon41(@(t) magic(2),[0,1],eye(4),100)
reference = [2.11697074177912, 1.96102704701374, 1.28551976108178, 0.587063762445168; ...
    2.61470272935166, 2.77064642411703, 0.782751683260224, 1.48120768189683; ...
    3.63377481086245, 5.03068680813567, 2.11697074177912, 1.96102704701374; ...
    6.70758241084756, 5.31067041357434, 2.61470272935166, 2.77064642411703];

d = 2;
result = upsilon41(@(t)eye(d), @(t)magic(d), [0, 1], [eye(d), zeros(d)], [zeros(d), eye(d)], 100);

assertElementsAlmostEqual(result, reference);
end

function test_upsilon41dec
% reference obtained with: upsilon41(@(t) magic(2),[0,1],eye(4),100)
reference = [2.11697074177912, 1.96102704701374, 1.28551976108178, 0.587063762445168; ...
    2.61470272935166, 2.77064642411703, 0.782751683260224, 1.48120768189683; ...
    3.63377481086245, 5.03068680813567, 2.11697074177912, 1.96102704701374; ...
    6.70758241084756, 5.31067041357434, 2.61470272935166, 2.77064642411703];

d = 2;
result = upsilon41(@(t)eye(d), @(t)magic(d), [0, 1], [eye(d), zeros(d)], [zeros(d), eye(d)], 100, 6);

assertElementsAlmostEqual(result, reference);
end

function test_upsilon62_vec
% reference obtained with: upsilon62(@(t) magic(2),[0,1],enes(4,1),100)
reference = [5.95058131231986; 7.64930851862588; 12.7424594077912; 17.4036019778908];

d = 2;
result = upsilon62(@(t)eye(d), @(t)magic(d), [0, 1], ones(2,1), ones(2,1), 100);

assertElementsAlmostEqual(result, reference);
end

function test_upsilon41_vec
% reference obtained with: upsilon41(@(t) magic(2),[0,1],eye(4),100)
reference = [5.95058131231980; 7.64930851862574; 12.7424594077910; 17.4036019778906];

d = 2;
result = upsilon41(@(t)eye(d), @(t)magic(d), [0, 1], ones(2,1), ones(2,1), 100);

assertElementsAlmostEqual(result, reference);
end

function test_upsilon62krylov_vec
% reference obtained with: upsilon41(@(t) magic(2),[0,1],eye(4),100)
reference = [5.95058131231980; 7.64930851862574; 12.7424594077910; 17.4036019778906];

d = 2;
result = upsilon62krylov(@(t)eye(d), @(t)magic(d), [0, 1], ones(2,1), ones(2,1), 100);

assertElementsAlmostEqual(result, reference);
end

function test_vector_solution
    d = 4;
v = rand(d, 1);
w = rand(d, 1);
iv = [v; w];

R = rand(d, d);
dg = diag(rand(d, 1)*d*d);
M = R' * R + dg;
I = eye(size(M));
O = zeros(size(M));

A = [O, I; -M, O];

h = 0.1;

ref = expm(A*h)*iv;
ups = upsilon62(I,-M,[0,h],v,w,10);

assertElementsAlmostEqual(ref,ups);
end