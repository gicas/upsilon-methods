function [v, w] = splitvec(vec)
% SPLITVEC splits an even-dimensional vector into halves
%   Detailed explanation goes here

len = length(vec);
if rem(len, 2) ~= 0
    error('The input vector has odd length');
end
half = len / 2;
v = vec(1:half,:);
w = vec(half+1:end,:);

end

