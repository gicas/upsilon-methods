function [result] = upsilon62krylov(A, B, t_span, iv_a, iv_b, n_steps, m)
% UPSILON62KRYLOV solves a split DE system using 6th-order Upsilon-method
% with Krylov subspace approximation.
%   solves a DE system split into I and G: [O,I;G,O] or, e.g.,
%   I and G:=-M: [O,I;-M,O].
%
%   This is an example implementation for illustrating the articles:%
%   1) Bader, P., Blanes, S., Ponsoda, E. & Seydaoğlu, M.
%   Symplectic integrators for the matrix Hill’s equation and its applications
%   to engineering models. (2015). at <http://arxiv.org/abs/1512.02343>
%   2) Bader, P., Blanes, S., Casas, F. Kopylov, N., Ponsoda, E.
%   Symplectic integrators for time-dependent linear wave equations. (2016).

addpath(genpath('./matrix-exp'));

B = any2fun(B);



% Gauss - Legendre quadrature points
c1 = 0.5 - sqrt(15.0) / 10.0;
c2 = 0.5;
c3 = 0.5 + sqrt(15.0) / 10.0;

I = eye(size(B(0)));
Z = zeros(size(B(0)));

t0 = t_span(1);
tf = t_span(end);
h = (tf - t0) / n_steps;

t = t0;
v = iv_a;
w = iv_b;

for step = 1:n_steps
    M1 = B(t+c1*h);
    M2 = B(t+c2*h);
    M3 = B(t+c3*h);
    
    K = -M1 + M3;
    L = M1 - 2 * M2 + M3;
    F = h * h * K * K;
    C1 = (-sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
    C2 = (+sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
    D1 = M2 - (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
    D2 = M2 + (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
    
    w = h * C1 * v + w;
    result = expmkr(0.5*h*[[Z, I]; [D1, Z]], [v; w], m);
    result = expmkr(0.5*h*[[Z, I]; [D2, Z]], result, m);
    [v, w] = splitvec(result);
    w = h * C2 * v + w;
    
    t = t + h;
end
result = [v; w];

end