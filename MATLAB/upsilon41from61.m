function [result] = upsilon41from61(f, t_span, init_val, steps_amb)
%UPSILON41FROM61 solves a split DE system using 4th-order Upsilon-method with
% one matrix exponential obtained from a 6th-order Upsilon-method.
%
%   [result] = upsilon41from61(f, tSpan, initVal, nSteps), with tSpan=[t0, tf]
%   and nSteps steps, integrates a DE y''=f(t)y using the 4th-order Upsilon-method
%   obtained from a 6th-order Upsilon-method with one matrix exponential,
%   discarding expensive exponential factors. initVal is the initial conditions
%   in matrix form.
%
%   This is an example implementation for illustrating the articles:
%   1) Bader, P., Blanes, S., Ponsoda, E. & Seydaoğlu, M.
%   Symplectic integrators for the matrix Hill’s equation and its applications
%   to engineering models. (2015). at <http://arxiv.org/abs/1512.02343>
%   2) Bader, P., Blanes, S., Casas, F. Kopylov, N., Ponsoda, E.
%   Symplectic integrators for time-dependent linear wave equations. (2016).

% cost = 46/3 + 2*2 + 1;

ST = dbstack;
disp(strcat('Solving with ', ST.name));
tic

% Gauss - Legendre quadrature points
c1 = 0.5 - sqrt(15.0) / 10.0;
c2 = 0.5;
c3 = 0.5 + sqrt(15.0) / 10.0;

% Sign change to transform from y''-f(t)=0 to y''=f(t)
f = @(t) - f(t);
I = eye(size(f(0)));
Z = zeros(size(f(0)));

t0 = t_span(1);
tf = t_span(2);

if steps_amb > 1
    n_steps = steps_amb;
elseif steps_amb <= 1
    n_steps = round((tf - t0)/steps_amb);
end
h = (tf - t0) / n_steps;

result = init_val;
t = t0;

for step = 1:n_steps
    M1 = f(t+c1*h);
    M2 = f(t+c2*h);
    M3 = f(t+c3*h);
    
    K = -(M3 - M1);
    L = -(M1 - 2 * M2 + M3);
    F = h^2 * K * K;
    
    C1 = -sqrt(15) / 36 * K + 1 / 18 * L - 1.0 / 864 * F;
    C2 = sqrt(15) / 36 * K + 1 / 18 * L - 1.0 / 864 * F;
    D = [[Z, I]; [-M2 + L ./ 6, Z]];
    
    result = [[I, Z]; [h * C2, I]] * expm(h*D) * [[I, Z]; [h * C1, I]] * result;
    
    t = t + h;
end

toc

end