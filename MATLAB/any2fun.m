function g = any2fun(f) 
g = f; 
if ~isa(f, 'function_handle') 
g = @(t) f; 
end 
end