function result = upsilon41(...
    F, G, t_span, iv_F, iv_G, steps_amb, varargin)
% UPSILON41 solves a split DE system using 6th-order with 1 expm.
%   UPSILON41(F, G, t_span, iv_F, iv_G, steps_amb, varargin)
%   integrates a DE y''+B(t)=0 by using (F is a placeholder)
%   the 6th-order Upsilon-method with one matrix exponential.
%   with the initial values IV_F and IV_G,
%   on T_SPAN, using time steps that are defined by STEPS_AMB.
%   VARARGIN corresponds to the order of decomposition of expms.
%
%   This is an example implementation for illustrating the articles:%
%   1) Bader, P., Blanes, S., Ponsoda, E. & Seydaoğlu, M.
%   Symplectic integrators for the matrix Hill’s equation and its applications
%   to engineering models. (2015). at <http://arxiv.org/abs/1512.02343>
%   2) Bader, P., Blanes, S., Casas, F. Kopylov, N., Ponsoda, E.
%   Symplectic integrators for time-dependent linear wave equations. (2016).

% cost = 46/3 + 2*2;

disp(['Solving with ', mfilename, '...']);
tic

% Gauss - Legendre quadrature points
c1 = 0.5 - sqrt(15.0) / 10.0;
c2 = 0.5;
c3 = 0.5 + sqrt(15.0) / 10.0;

% Sign change to transform from y''-f(t)=0 to y''=f(t)
G = @(t) - G(t);
I = eye(size(G(0)));
O = zeros(size(G(0)));

t0 = t_span(1);
tf = t_span(end);
if steps_amb > 1
    n_steps = steps_amb;
elseif steps_amb <= 1
    n_steps = (tf - t0) / steps_amb;
end
h = (tf - t0) / n_steps;

result = [iv_F; iv_G];
t = t0;

for step = 1:n_steps
    t = t0 + (step - 1) * h;
    
    M1 = G(t+c1*h);
    M2 = G(t+c2*h);
    M3 = G(t+c3*h);
    
    K = (M1 - M3) * (sqrt(15) / 36);
    L = -(M1 - 2 * M2 + M3) * (5 / 36);
    D = -M2;
    
    if nargin == 5
        dec_order = varargin{1};
        [Q, R] = dec_expm(D, h, dec_order);
        result = [[I, O]; [R + (K + L) * h, I]] * [[I, Q]; [O, I]] * ...
            [[I, O]; [R + (-K + L) * h, I]] * result;
    else
        result = [[I, O]; [(K + L) * h, I]] * expm(h*[[O, I]; [D, O]]) * ...
            [[I, O]; [(-K + L) * h, I]] * result;
    end    
end

toc
% end of function
end