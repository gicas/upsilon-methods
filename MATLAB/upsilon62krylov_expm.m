function [result] = upsilon62krylov_expm(A, B, t_span, iv_a, iv_b, steps_amb)
%UPSILON62 solves a split DE system using 6th-order Upsilon-method with
% two matrix exponentials.
%
%   [result] = upsilon62(f, tSpan, initVal, nSteps), with TSPAN=[t0, tf] and
%   nSteps steps, integrates a DE y''=f(t)y using the 6th-order Upsilon-method
%   with two matrix exponentials. initVal is the initial conditions in matrix
%   form.
%
%   This is an example implementation for illustrating the articles:%
%   1) Bader, P., Blanes, S., Ponsoda, E. & Seydaoğlu, M.
%   Symplectic integrators for the matrix Hill’s equation and its applications
%   to engineering models. (2015). at <http://arxiv.org/abs/1512.02343>
%   2) Bader, P., Blanes, S., Casas, F. Kopylov, N., Ponsoda, E.
%   Symplectic integrators for time-dependent linear wave equations. (2016).

addpath(genpath('./matrix-exp'));

ST = dbstack;
disp(strcat('Solving with ', ST.name));
tic

% Gauss - Legendre quadrature points
c1 = 0.5 - sqrt(15.0) / 10.0;
c2 = 0.5;
c3 = 0.5 + sqrt(15.0) / 10.0;

% Sign change to transform from y''-f(t)=0 to y''=f(t)
B = @(t) - B(t);
I = eye(size(B(0)));
Z = zeros(size(B(0)));

t0 = t_span(1);
tf = t_span(2);

if steps_amb > 1
    n_steps = steps_amb;
elseif steps_amb <= 1
    n_steps = round((tf - t0)/steps_amb);
end
h = (tf - t0) / n_steps;

result = [iv_a; iv_b];
t = t0;

for step = 1:n_steps
    M1 = B(t+c1*h);
    M2 = B(t+c2*h);
    M3 = B(t+c3*h);
    
    K = M1 - M3;
    L = -(M1 - 2 * M2 + M3);
    F = h * h * K * K;
    C1 = (-sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
    C2 = (+sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
    D1 = -M2 - (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
    D2 = -M2 + (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
    
    result = [[I, Z]; [h * C2, I]] * krylovExpmMatrix(0.5*h*[[Z, I]; [D2, Z]]) * ...
        krylovExpmMatrix(0.5*h*[[Z, I]; [D1, Z]]) * [[I, Z]; [h * C1, I]] * result;
    
    t = t + h;
end

toc

end