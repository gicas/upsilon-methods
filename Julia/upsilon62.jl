function upsilon62(plc,G,t_span,iv_F,iv_G,n_steps)
    c1 = 0.5 - sqrt(15.0) / 10.0;
    c2 = 0.5;
    c3 = 0.5 + sqrt(15.0) / 10.0;

    Id = eye(size(G)[1]);
    O = zeros(size(G));

    t0 = t_span[1];
    tf = t_span[end];
    h = (tf - t0) / n_steps;

    V = iv_F;
    W = iv_G;

    for step in 1:n_steps
        t = t0 + (step - 1) * h;
        
        M1 = G;
        M2 = G;
        M3 = G;
        
        K = -M1 + M3;
        L = M1 - 2 * M2 + M3;
        F = h * h * K * K;
        C1 = (-sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
        C2 = (+sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
        D1 = M2 - (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
        D2 = M2 + (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
        
        result = [V; h * C1 * V + W];
        result = expm(0.5*h*[O Id; D1 O]) * result;
        result = expm(0.5*h*[O Id; D2 O]) * result;
        result = [V; h * C2 * V + W];
    end

    return [V; W];
end
