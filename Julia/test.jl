function upsilon62(plc,G,t_span,f,g,n_steps)
    c1 = 0.5 - sqrt(15.0) / 10.0;
    c2 = 0.5;
    c3 = 0.5 + sqrt(15.0) / 10.0;

    Id = eye(size(G)[1]);
    O = zeros(size(G));

    t0 = t_span[1];
    tf = t_span[2];
    h = (tf - t0) / n_steps;

    result = [f;g];
    for step in 1:n_steps
        M1 = G;
        M2 = G;
        M3 = G;        
        K = -M1 + M3;
        L = M1 - 2 * M2 + M3;
        F = h * h * K * K;
        C1 = (-sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
        C2 = (+sqrt(15.0) / 180.0) * K + (1.0 / 18.0) * L + (1.0 / 12960.0) * F;
        D1 = M2 - (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
        D2 = M2 + (4.0 / (3 * sqrt(15.0))) * K + (1.0 / 6.0) * L;
        
        result = [Id O; h*C1 Id]*result;
        result = expm(0.5*h*[O Id; D1 O]) * result;
        result = expm(0.5*h*[O Id; D2 O]) * result;
        result = [Id O; h*C2 Id]*result;     
    end

    return result;
end

nx = 8;
R = rand(nx,nx);
r= norm(R,2);
dg = rand(nx) * nx * 2;
tmp = R*R'*diagm(dg,0);

M = tmp / sqrt(r);
N = -M;
Id = eye(nx,nx);
O = zeros(nx,nx)
A = [O Id; N O];

v = rand(nx);
w = rand(nx);
iv = [v; w];

t0=0.0;
T = 1.0;
n_steps = 10;
h = (T-t0)/n_steps;

f1 = v;
g1 = w;
for s in 1:n_steps
    ti = (s-1)*h;
    tf = s*h;
    ref = expm(A*(tf-t0))*iv;
    sol = upsilon62(I,N,[ti,tf],f1,g1,1);
    f1 = sol[1:length(v)];
    g1 = sol[length(v)+1:end];
    dif = norm(ref-sol,2);
    println(dif);
end